.. Arcade documentation master file, created by
   sphinx-quickstart on Mon Dec 28 23:02:22 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Arcade's documentation!
==================================

Arcade is an easy-to-learn Python library for creating 2d video games.

The source is available on GitHub: https://github.com/pvcraven/arcade

.. toctree::
   :maxdepth: 2

   arcade
   installation
   examples

.. automodule:: arcade

Indexes and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

