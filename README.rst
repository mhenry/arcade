Welcome to Arcade's documentation!
==================================

|build-status-travis| |build-status-appveyor| |docs| |coverage| |version|

Arcade is an easy-to-learn Python library for creating 2d video games.

About Arcade Game Library
=========================
Arcade Game Library is a wrapper on top of OpenGL.

For full documentation see http://pythonhosted.org/arcade/

License
=======
This library is given to the public domain. There are no licensing
restrictions.

.. |build-status-travis| image:: https://travis-ci.org/pvcraven/arcade.svg
    :target: https://travis-ci.org/pvcraven/arcade
    :alt: build status
    :scale: 100%

.. |build-status-appveyor| image:: https://ci.appveyor.com/api/projects/status/github/pvcraven/arcade
    :target: https://ci.appveyor.com/project/pvcraven/arcade-ekjdf
    :alt: build status
    :scale: 100%

.. |docs| image:: https://readthedocs.org/projects/arcade/badge/?version=latest
    :alt: Documentation Status
    :scale: 100%
    :target: https://arcade.readthedocs.org/en/latest/?badge=latest

.. |coverage| image:: https://coveralls.io/repos/pvcraven/arcade/badge.svg?branch=master&service=github
    :alt: Test Coverage Status
    :scale: 100%
    :target: https://coveralls.io/github/pvcraven/arcade?branch=master
.. |version| image:: https://img.shields.io/pypi/v/arcade.svg   
    :alt: Version
    :scale: 100%
    :target: https://pypi.python.org/pypi/arcade

